
	export const state = () => ({
		items:[
			{
				id: 1,
				name: "Iphone XR",
				price: 49990,
				select: false,
				num: 0,
				img: "xr.jpg"
			},
			{
				id: 2,
				name: "Iphone X",
				price: 40000,
				select: false,
				num: 0,
				img: "x.jpg"

			},
			{
				id: 3,
				name: "Iphone XS",
				price: 59990,
				select: false,
				num: 0,
				img: "xs.jpg"
			},
			{
				id: 4,
				name: "Iphone XS MAX",
				price: 70000,
				select: false,
				num: 0,
				img: "xsm.jpg"
			},
			{
				id: 5,
				name: "Iphone 7",
				price: 35750,
				select: false,
				num: 0,
				img: "7.jpg"
			},
			{
				id: 6,
				name: "Iphone 8",
				price: 38588,
				select: false,
				num: 0,
				img: "8.jpg"
			},
		],
		numBadge: 0
	});

	export const getters = {
		getItems(state){
			return state.items;
		},
		getNumBadge(state){
			return state.numBadge;
		},
		subTotal(state,index){
			return state.items[index].price * state.items[index].num;
		}
	}

	export const mutations = {
		addToCart(state,index){
			state.items[index].select = true;
			state.items[index].num = 1;
			state.numBadge++;
		},
		itemPlus(state,index){
			state.items[index].num++;
		},
		itemMin(state,index){
			state.items[index].num--;
			if(state.items[index].num <= 0){
				state.items[index].num = 0;
			}
		},
		deleteItem(state,index){
			state.items[index].select = false;
			state.numBadge--;
		},
		priceSort(state){
			return state.items.sort((a,b)=>{
				return a.price - b.price;
			});
		}
	}
